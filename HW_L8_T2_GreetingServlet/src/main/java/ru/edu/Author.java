package ru.edu;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.Writer;

@WebServlet("/author/*")
public class Author extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        resp.setCharacterEncoding("UTF-8");

        Writer writer = resp.getWriter();

        writer.write("<!doctype html>");
        writer.write("<html>");
        writer.write("<head>");
        writer.write("<meta charset=\"utf-8\">");
        writer.write("</head>");
        writer.write("<body>");
        writer.write("<h2>Информация об авторе:</h2>");
        writer.write("<h3> Фамилия: Микишев <br>");
        writer.write("Имя: Артем <br>");
        writer.write("Отчество: Дмитриевич <br>");
        writer.write("Телефон: 8-800-535-35-35 <br>");
        writer.write("Хобби: шахматы, разработка <br>");
        writer.write("Bitbucket url: <a href=\"https://bitbucket.org/artemmikishev\"> https://bitbucket.org/artemmikishev </a> </h3>");
        writer.write("</body>");
        writer.write("</html>");
/**
<html>
    <body>
        <h2>Информация об авторе:</h2>
        <h3>
                Фамилия: Микишев <br>
        Имя: Артем <br>
        Отчество: Дмитриевич <br>
        Телефон: 8-800-535-35-35 <br>
                Хобби: шахматы, разработка <br>
                Bitbucket url: <a href="https://bitbucket.org/artemmikishev"> this </a>
        </h3>
    </body>
</html>
 */
    }
}
