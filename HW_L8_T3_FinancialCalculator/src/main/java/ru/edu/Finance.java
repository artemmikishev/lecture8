package ru.edu;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.Writer;


public class Finance extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        resp.setCharacterEncoding("UTF-8");

        Writer writer = resp.getWriter();

        writer.write("<!doctype html>");
        writer.write("<html>");
        writer.write("<head>");
        writer.write("<meta charset=\"utf-8\">");
        writer.write("</head>");
        writer.write("<body>");
        writer.write("<center>");
        writer.write("<h1> Калькулятор доходности вклада </h1>");
        writer.write("<form method=\"POST\" action=\"\">");
        writer.write("<h3><table><tbody>");
        writer.write("<tr><td>Сумма на момент открытия</td> <td><input name=\"summary\"> </td></tr>");
        writer.write("<tr><td>Процентная ставка </td> <td> <input name=\"percent\"> </td></tr>");
        writer.write("<tr><td>Количество лет </td> <td> <input name=\"years\"> </td></tr>");
        writer.write("<tr><td><input type=\"submit\" value=\"Посчитать\" color=\"blue\"> </td></tr>");
        writer.write("</table></tbody></h3>");
        writer.write("</form");
        writer.write("</center>");
        writer.write("</body>");
        writer.write("</html>");
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        String sum = req.getParameter("summary");
        String percentage = req.getParameter("percent");
        String years = req.getParameter("years");
        resp.setCharacterEncoding("UTF-8");

        try {
            int summary = Integer.parseInt(sum);
            double percent = Double.parseDouble(percentage);
            int yearsPer = Integer.parseInt(years);
            if (summary < 50000) {
                Writer writer = resp.getWriter();
                writer.write("<!doctype html>");
                writer.write("<html>");
                writer.write("<head>");
                writer.write("<meta charset=\"utf-8\">");
                writer.write("</head>");
                writer.write("<body>");
                writer.write("Минимальная сумма на момент открытия вклада 50 000 рублей");
                writer.write("</body>");
                writer.write("</html>");
            } else  if (percent > 0 && yearsPer > 0){
                double result = summary/100 * percent * yearsPer;
                Writer writer = resp.getWriter();

                writer.write("<!doctype html>");
                writer.write("<html>");
                writer.write("<head>");
                writer.write("<meta charset=\"utf-8\">");
                writer.write("</head>");
                writer.write("<body>");
                writer.write("<center>");
                writer.write("<h1> Калькулятор доходности вклада </h1>");
                writer.write("<h3><table><tbody>");
                writer.write("<tr><td>Сумма на момент открытия</td> <td>" + sum + " </td></tr>");
                writer.write("<tr><td>Процентная ставка </td> <td> " + percentage +" </td></tr>");
                writer.write("<tr><td>Количество лет </td> <td> " + years + " </td></tr>");
                writer.write("<tr><td><input type=\"submit\" value=\"Посчитать\"> </td> <td> " + result + "</td> </tr>");
                writer.write("</table></tbody></h3>");
                writer.write("</form");
                writer.write("</center>");
                writer.write("</body>");
                writer.write("</html>");
            } else
                throw new IOException();
        } catch (Exception ex) {
            Writer writer = resp.getWriter();
            writer.write("<!doctype html>");
            writer.write("<html>");
            writer.write("<head>");
            writer.write("<meta charset=\"utf-8\">");
            writer.write("</head>");
            writer.write("<body>");
            writer.write("Неверный формат данных. Скорректируйте значения");
            writer.write("</body>");
            writer.write("</html>");
        }



    }
}
